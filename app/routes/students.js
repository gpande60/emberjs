import Route from '@ember/routing/route';
import { action } from '@ember/object';

export default class StudentsRoute extends Route {
  async model() {
    const response = await fetch('http://localhost:8080/students/');
    const obj = await response.json();
    return obj;
  }
  @action
  async onSubmit() {
    let student = {
      name: this.controller.get('name'),
      email: this.controller.get('email'),
      studentNumber: this.controller.get('studentNumber'),
      gpa: this.controller.get('gpa'),
    };
    console.log(student);
    // let fetchObject = {
    //   method: 'POST',
    //   cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
    //   headers : {
    //     'Content-type' : 'application/json',
    //   },
    //   body : JSON.stringify(student),
    // };
    return fetch('http://localhost:8080/students/save', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(student),
    })
      .then((response) => {
        if (!response.ok) {
          throw new Error('Network response was not ok');
        }
        this.refresh();
      })
      .catch((error) => {
        alert(`There has been a problem with your fetch operation: ${error}`);
      });
  }
  @action
  async onDelete(name) {
    console.log(name);
    // Awaiting fetch which contains
    // method, headers and content-type
    return fetch('http://localhost:8080/students/delete/' + name, {
      method: 'DELETE',
      headers: {
        'Content-type': 'application/json',
      },
    })
      .then((response) => {
        this.refresh();
      })
      .catch((error) => {
        alert(`There has been a problem with your fetch operation: ${error}`);
      });
  }
  @action
  async getStudent(email) {
    const response = await fetch(
      'http://localhost:8080/students/byEmail/' + email
    );
    const obj = await response.json();
    if (obj) console.log(obj);
    else console.log(false);
  }
}
