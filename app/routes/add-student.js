import Route from '@ember/routing/route';
import { action } from '@ember/object';

export default class AddStudentRoute extends Route {
  @action
  async onSubmit() {
    let student = {
      name: this.controller.get('name'),
      email: this.controller.get('email'),
      studentNumber: this.controller.get('studentNumber'),
      gpa: this.controller.get('gpa'),
    };
    console.log(student);
    // let fetchObject = {
    //   method: 'POST',
    //   cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
    //   headers : {
    //     'Content-type' : 'application/json',
    //   },
    //   body : JSON.stringify(student),
    // };
    return fetch('http://localhost:8080/students/save', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(student),
    })
      .then((response) => {
        if (!response.ok) {
          throw new Error('Network response was not ok');
        }
        this.transitionTo('index');
      })
      .catch((error) => {
        alert(`There has been a problem with your fetch operation: ${error}`);
      });
  }
}
