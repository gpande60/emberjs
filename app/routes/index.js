import Route from '@ember/routing/route';
import { action } from '@ember/object';

export default class IndexRoute extends Route {
  async model() {
    const response = await fetch('http://localhost:8080/students/');
    const obj = await response.json();
    return obj;
  }
  @action
  async onDelete(email) {
    const response = await fetch(
      'http://localhost:8080/students/byEmail/' + email
    );
    const obj = await response.json();
    if (obj) {
      return fetch('http://localhost:8080/students/delete/' + obj.name, {
        method: 'DELETE',
        headers: {
          'Content-type': 'application/json',
        },
      })
        .then((response) => {
          this.refresh();
        })
        .catch((error) => {
          alert(`There has been a problem with your fetch operation: ${error}`);
        });
    } else {
      alert(`There has been a problem with your fetch operation`);
    }
  }
}
